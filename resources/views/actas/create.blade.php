@extends('layouts.app')

@section('content')
<div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom ">
            <ul class="nav nav-tabs ">
              <li class="active "><a href="#tab_1" data-toggle="tab" aria-expanded="true"><i class="fa fa-user"></i> Paciente</a></li>
              <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false"><i class="fa fa-heartbeat"></i> Actividad Diaria</a></li>
              <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false"><i class="fa fa-file-excel-o"></i> Historial Clinico</a></li>

            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                <form role="form">
                  <div class="box-body">

                    <div class="row">

                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputPassword1">Paciente</label>
                          <select name="paciente_id" class="form-control">
                            <option value="">Selecciona un Paciente</option>
                            @foreach ($clientes as $cliente)
                                <option value="{{ $cliente->id }}"> {{ $cliente->nombre }} {{ $cliente->apellido_paterno }} {{ $cliente->apellido_materno }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputPassword1">Tipo de Sangre</label>
                          <input type="text" class="form-control" name="sangre" placeholder="Apellido Paterno" value="">
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputPassword1">biompedancia</label>
                          <input type="text" class="form-control" name="apellido_materno" placeholder="Introducir Tiempo" value="">
                        </div>
                      </div>
                      
                    </div>

                    <div class="row">

                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputPassword1">¿Fuma Cigarro ?</label>
                          <select name="" class="form-control">
                            <option value="">Selecciona</option>
                            <option value="1">SI</option>
                            <option value="2">NO</option>
                          </select>
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputPassword1">¿Cada Cuanto Fuma?</label>
                          <input type="text" class="form-control" name="telefono" placeholder="Introducir Tiempo" value="">
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputPassword1">¿Toma Alcohol ?</label>
                          <select name="" class="form-control">
                            <option value="">Selecciona</option>
                            <option value="1">SI</option>
                            <option value="2">NO</option>
                          </select>
                        </div>
                      </div>


                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="exampleInputPassword1">¿Cada Cuanto Toma Alcohol ?</label>
                          <input type="text" class="form-control" id="datepicker" name="fecha_nacimiento" placeholder="Introducir Tiempo" value="">
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="exampleInputPassword1">¿Se Drogra ?</label>
                          <select name="" class="form-control">
                            <option value="">Selecciona</option>
                            <option value="1">Si</option>
                            <option value="2">NO</option>
                          </select>
                        </div>
                      </div>

                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="exampleInputPassword1">¿Cada Cuanto Se Droga ?</label>
                          <input type="text" class="form-control" id="datepicker" name="fecha_nacimiento" placeholder="Introducir Tiempo" value="">
                        </div>
                      </div>


                    </div>
                    
                  </div>

                </form>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <form role="form">
                  <div class="box-body">

                    <div class="row">

                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputPassword1">¿Usted Trabaja ?</label>
                          <select name="" class="form-control">
                            <option value="">Selecciona</option>
                            <option value="1">SI</option>
                            <option value="2">NO</option>
                          </select>
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputPassword1">Descripción del Trabajo</label>
                          <input type="text" class="form-control" name="sangre" placeholder="Apellido Paterno" value="">
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputPassword1">¿Cuantas horas Trabaja?</label>
                          <select name="" class="form-control">
                            <option value="">Selecciona</option>
                            <option value="1">1 Hora</option>
                            <option value="2">2 Hora</option>
                            <option value="3">3 Hora</option>
                            <option value="4">4 Hora</option>
                            <option value="5">5 Hora</option>
                            <option value="6">6 Hora</option>
                            <option value="7">7 Hora</option>
                            <option value="8">8 Hora</option>
                            <option value="9">Mas de 8 Horas</option>

                          </select>
                        </div>
                      </div>
                      
                    </div>

                    <div class="row">

                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputPassword1">¿Realiza alguna Actividad Fisica ?</label>
                          <select name="" class="form-control">
                            <option value="">Selecciona</option>
                            <option value="1">SI</option>
                            <option value="2">NO</option>
                          </select>
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputPassword1">Que Actividad Fisica</label>
                          <input type="text" class="form-control" name="telefono" placeholder="Introducir Tiempo" value="">
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputPassword1">¿Cuanto tiempo ?</label>
                          <select name="" class="form-control">
                            <option value="">Selecciona</option>
                            <option value="1">30 min</option>
                            <option value="2">1 Hora</option>
                            <option value="3">Mas de 1 Hora</option>
                          </select>
                        </div>
                      </div>


                    </div>

                    <div class="row">


                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="exampleInputPassword1">¿Se Estresa ?</label>
                          <select name="" class="form-control">
                            <option value="">Selecciona</option>
                            <option value="1">Si</option>
                            <option value="2">NO</option>
                          </select>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="exampleInputPassword1">¿Motivos del Estres ?</label>
                          <input type="text" class="form-control" id="datepicker" name="fecha_nacimiento" placeholder="Introducir Tiempo" value="">
                        </div>
                      </div>


                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="exampleInputPassword1">¿Cada Cuanto Se Estresa ?</label>
                          <select name="" class="form-control">
                            <option value="">Selecciona</option>
                            <option value="1">Diario</option>
                            <option value="2">Cada 3 Dia</option>
                            <option value="3">1 vez a la semana</option>
                          </select>
                        </div>
                      </div>


                    </div>
                    
                  </div>

                </form>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
                <form role="form">
                  <div class="box-body">

                    <div class="row">

                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputPassword1">¿Usted Trabaja ?</label>
                          <select name="" class="form-control">
                            <option value="">Selecciona</option>
                            <option value="1">SI</option>
                            <option value="2">NO</option>
                          </select>
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputPassword1">Descripción del Trabajo</label>
                          <input type="text" class="form-control" name="sangre" placeholder="Apellido Paterno" value="">
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputPassword1">¿Cuantas horas Trabaja?</label>
                          <select name="" class="form-control">
                            <option value="">Selecciona</option>
                            <option value="1">1 Hora</option>
                            <option value="2">2 Hora</option>
                            <option value="3">3 Hora</option>
                            <option value="4">4 Hora</option>
                            <option value="5">5 Hora</option>
                            <option value="6">6 Hora</option>
                            <option value="7">7 Hora</option>
                            <option value="8">8 Hora</option>
                            <option value="9">Mas de 8 Horas</option>

                          </select>
                        </div>
                      </div>
                      
                    </div>

                    <div class="row">

                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputPassword1">¿Realiza alguna Actividad Fisica ?</label>
                          <select name="" class="form-control">
                            <option value="">Selecciona</option>
                            <option value="1">SI</option>
                            <option value="2">NO</option>
                          </select>
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputPassword1">Que Actividad Fisica</label>
                          <input type="text" class="form-control" name="telefono" placeholder="Introducir Tiempo" value="">
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputPassword1">¿Cuanto tiempo ?</label>
                          <select name="" class="form-control">
                            <option value="">Selecciona</option>
                            <option value="1">30 min</option>
                            <option value="2">1 Hora</option>
                            <option value="3">Mas de 1 Hora</option>
                          </select>
                        </div>
                      </div>


                    </div>

                    <div class="row">


                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="exampleInputPassword1">¿Se Estresa ?</label>
                          <select name="" class="form-control">
                            <option value="">Selecciona</option>
                            <option value="1">Si</option>
                            <option value="2">NO</option>
                          </select>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="exampleInputPassword1">¿Motivos del Estres ?</label>
                          <input type="text" class="form-control" id="datepicker" name="fecha_nacimiento" placeholder="Introducir Tiempo" value="">
                        </div>
                      </div>


                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="exampleInputPassword1">¿Cada Cuanto Se Estresa ?</label>
                          <select name="" class="form-control">
                            <option value="">Selecciona</option>
                            <option value="1">Diario</option>
                            <option value="2">Cada 3 Dia</option>
                            <option value="3">1 vez a la semana</option>
                          </select>
                        </div>
                      </div>


                    </div>
                    
                  </div>

                </form>
              </div>


              <!-- /.tab-pane -->
              <div class="box-footer">
                <a href="{{ url('actas') }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Regresar</a>
                @isset($clientess)
                <button type="submit" class="btn btn-success btn-submit"><i class="fa fa-repeat"></i> Actualizar</button>
                @else
                <button type="submit" class="btn btn-success btn-submit"><i class="fa fa-save"></i> Agregar</button>
                @endisset
              </div>
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
@endsection