-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-05-2020 a las 23:12:20
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_nutri_saludable`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alimento`
--

CREATE TABLE `alimento` (
  `id` int(11) NOT NULL,
  `nombre` varchar(65) DEFAULT NULL,
  `agua` float(6,2) DEFAULT NULL,
  `azucar` float(6,2) DEFAULT NULL,
  `hdec` float(6,2) DEFAULT NULL,
  `lipidos` float(6,2) DEFAULT NULL,
  `proteina` float(6,2) DEFAULT NULL,
  `fibra` float(6,2) DEFAULT NULL,
  `calcio` float(6,2) DEFAULT NULL,
  `hierro` float(6,2) DEFAULT NULL,
  `magnesio` float(6,2) DEFAULT NULL,
  `fosforo` float(6,2) DEFAULT NULL,
  `potasio` float(6,2) DEFAULT NULL,
  `sodio` float(6,2) DEFAULT NULL,
  `cobre` float(6,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `apellido_paterno` varchar(200) DEFAULT NULL,
  `apellido_materno` varchar(200) DEFAULT NULL,
  `correo_electronico` varchar(50) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `direccion` text DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `peso` varchar(2) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `nombre`, `apellido_paterno`, `apellido_materno`, `correo_electronico`, `telefono`, `direccion`, `fecha_nacimiento`, `peso`, `password`, `activo`, `created_at`, `updated_at`) VALUES
(1, 'Leonardo', 'Amaya', 'Escobedo', 'leoameb@gmail.com', '8342732302', 'Calle cardenas #456 Las lomas Ciudad Victoria,Tamaulipas', '2000-06-13', '76', NULL, 1, '2020-02-28 04:40:03', '2020-02-28 04:40:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente_promocion`
--

CREATE TABLE `cliente_promocion` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_promocion` int(11) DEFAULT NULL,
  `clientes_promocion` text DEFAULT NULL,
  `activo` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cliente_promocion`
--

INSERT INTO `cliente_promocion` (`id`, `id_promocion`, `clientes_promocion`, `activo`, `created_at`, `updated_at`) VALUES
(1, 1, 'leoameb@gmail.com', 1, '2020-02-28 04:46:28', '2020-02-28 04:46:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingresos`
--

CREATE TABLE `ingresos` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `monto` float NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promocion`
--

CREATE TABLE `promocion` (
  `id` int(11) UNSIGNED NOT NULL,
  `nombre_promocion` varchar(100) DEFAULT NULL,
  `clientes_promocion` text DEFAULT NULL,
  `descuentos_promocion` int(11) DEFAULT NULL,
  `descripcion` text DEFAULT NULL,
  `template` int(11) DEFAULT NULL,
  `serie_promocion` int(11) DEFAULT NULL,
  `fecha_inicial_vigencia` date DEFAULT NULL,
  `fecha_final_vigencia` date DEFAULT NULL,
  `activo` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `promocion`
--

INSERT INTO `promocion` (`id`, `nombre_promocion`, `clientes_promocion`, `descuentos_promocion`, `descripcion`, `template`, `serie_promocion`, `fecha_inicial_vigencia`, `fecha_final_vigencia`, `activo`, `created_at`, `updated_at`) VALUES
(1, 'Cuida tu Salud', NULL, 40, 'Ven a nuestra tienda por servicios para tu salud', 1, 526, '2020-02-27', '2020-03-13', 1, '2020-02-28 04:46:28', '2020-02-28 04:46:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password_name` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo_user` int(11) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activo` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `password_name`, `tipo_user`, `id_cliente`, `remember_token`, `activo`, `created_at`, `updated_at`) VALUES
(1, 'hector89', 'hector@hotmail.com', '$2y$10$rxr0NeYXUf.Xjznz9u9Jn.Vm7vEQpn75WQBiHFiX1SUFtGRYUWvIq', NULL, 1, 0, 'CQlXwcgED4oKOkgyAU9pqgI0VDo5pS94ucjOVBEb6ngu0OB5dgnSErSlmVBA', 1, '2020-02-27 01:27:59', '2020-02-27 01:27:59'),
(10, 'diego.salmedo', 'hector_vargas89@hotmail.com', '$2y$10$jPJfJseBDqWxJaTtU7SJx.maboAs0AcbiUAdG1Vq/MPlehkhkS.te', 'diego89', 2, NULL, NULL, 0, '2020-02-28 02:30:28', '2020-02-28 04:28:59'),
(11, 'Leo.Ameb', 'leoameb@gmail.com', '$2y$10$h8HQ6RujywsfjnM8zDq4beLYMyGH5XVq484Q2reuoncUuFAYJ9bna', 'leo.ameb', 1, NULL, NULL, 1, '2020-02-28 04:30:37', '2020-02-28 04:30:37');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alimento`
--
ALTER TABLE `alimento`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cliente_promocion`
--
ALTER TABLE `cliente_promocion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ingresos`
--
ALTER TABLE `ingresos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `promocion`
--
ALTER TABLE `promocion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alimento`
--
ALTER TABLE `alimento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cliente_promocion`
--
ALTER TABLE `cliente_promocion`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `ingresos`
--
ALTER TABLE `ingresos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `promocion`
--
ALTER TABLE `promocion`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
